package com.linkmatm.service.mPos;

import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface PosApiInterface {
    /* @Headers({
             "content-type: application/json;charset=UTF-8"
     })*/
    @POST("/doCashWithdral")
    //@POST("/IttyMAppAuth/doCashWithdral")
    Call<PosTransResponse> SendTransRequest(@Header("Authorization") String token,@Body JsonObject posTransRequest);



    /* @Headers({
             "content-type: application/json;charset=UTF-8"
     })*/
    @POST("/doBalanceInquiry")
    //@POST("/IttyMAppAuth/doBalanceInquiry")
    Call<PosTransResponse> SendTransRequestBalanceInq(@Header("Authorization") String token,@Body JsonObject posTransRequest);
}
