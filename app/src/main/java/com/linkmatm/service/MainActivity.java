package com.linkmatm.service;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {
    int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
        checkLocation();

        Bundle b = getIntent().getExtras();
        if (b != null) {
            String activityName = b.getString("ActivityName");
            if (activityName.equals("Bluetooth")) {
                Constants.applicationType = b.getString("ApplicationType");
                if (Constants.applicationType.equals("CORE")) {
                    Constants.token = b.getString("UserToken");
                    Constants.user_id = b.getString("UserName");
                } else {
                    Constants.loginID = b.getString("LoginID");
                    Constants.encryptedData = b.getString("EncryptedData");
                }
                Intent intent = new Intent(MainActivity.this, BluetoothActivity.class);
                startActivity(intent);
                finish();
            } else if (activityName.equals("mATM2")) {
                if (PosActivity.isBlueToothConnected(this)) {
                    Constants.applicationType = b.getString("ApplicationType");
                    if (Constants.applicationType.equals("CORE")) {
                        Constants.token = b.getString("UserToken");
                        Constants.user_id = b.getString("UserName");
                        Constants.tokenFromCoreApp = b.getString("UserToken");
                        Constants.userNameFromCoreApp = b.getString("UserName");
                    } else {
                        Constants.loginID = b.getString("LoginID");
                        Constants.encryptedData = b.getString("EncryptedData");
                    }

                    Constants.paramA = b.getString("ParamA");
                    Constants.paramB = b.getString("ParamB");
                    Constants.paramC = b.getString("ParamC");

                    Constants.transactionType = b.getString("TransactionType");
                    Constants.transactionAmount = b.getString("Amount");

                    Intent intent = new Intent(MainActivity.this, PosActivity.class);
                    startActivity(intent);
                } else {
                    showAlertDialog("Please pair the bluetooth device");
                }
            } else {
                showAlertDialog("Invalid transaction data, Please use this app with parent application");
            }
        } else {
            showAlertDialog("Invalid transaction data, Please use this app with parent application");
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent i = Constants.intentData;
        if (Constants.isPos.equals(Constants.statusPos)) { //opens status activity in sdk
            setResult(RESULT_OK, i);
            Constants.initResult();
            finish();
        } else if (Constants.isPos.equals(Constants.error2Pos)) { //opens error2 activity in sdk
            setResult(RESULT_OK, i);
            Constants.initResult();
            finish();
        } else if (Constants.isPos.equals(Constants.errorPos)){ //opens error activity in sdk
            Intent in = new Intent();
            in.putExtra("error1Response", i.getIntExtra("errorResponse",0));
            setResult(RESULT_OK, in);
            Constants.initResult();
            finish();
        } else if (Constants.isPos.equals(Constants.eventPos)){ //closes service and sdk
            Constants.initResult();
            finish();
        }
    }

    public void showAlertDialog(final String msg) {
        if (!isFinishing()) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    AlertDialog.Builder alertbuilderupdate;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        alertbuilderupdate = new AlertDialog.Builder(MainActivity.this, android.R.style.Theme_Material_Light_Dialog_Alert);
                    } else {
                        alertbuilderupdate = new AlertDialog.Builder(MainActivity.this);
                    }
                    alertbuilderupdate.setCancelable(false);
                    // String message = "A new version of "+ getResources().getString(R.string.app_name)+" is available. Please update to version "+latestVersion +" "+ getResources().getString(R.string.youAreNotUpdatedMessage1);
                    alertbuilderupdate.setTitle("Alert")
                            .setMessage(msg)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                    finishAffinity();
                                    System.exit(0);
                                }
                            })
                            .show();
                }
            });
        }
    }

    public void checkLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_LOCATION);

        } else {
            // No explanation needed, we can request the permission.
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_LOCATION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == MY_PERMISSIONS_REQUEST_LOCATION) {
            // If request is cancelled, the result arrays are empty.
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                // permission was granted, yay! Do the
                // location-related task you need to do.
                if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {

                    //Request location updates:
                    forceUpdate();

                }

            } else {

                forceUpdate();
                // permission denied, boo! Disable the
                // functionality that depends on this permission.

            }
            return;
        }

    }

    public void forceUpdate() {
        PackageManager packageManager = this.getPackageManager();
        PackageInfo packageInfo = null;

        try {
            packageInfo = packageManager.getPackageInfo(getPackageName(), 0);

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        String currentVersion = packageInfo.versionName;

        new ForceUpdateAsync(currentVersion, MainActivity.this, new ForceUpdateAsync.AsyncListener() {
            @Override
            public void doStuff(String latestVersion, String currentVersion) {
                if (latestVersion != null) {
                    if (!currentVersion.equalsIgnoreCase(latestVersion)) {
                        showForceUpdateDialog(latestVersion);
                    }
                }
            }
        }).execute();
    }

    public void showForceUpdateDialog(final String latestVersion) {

        if (!isFinishing()) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    AlertDialog.Builder alertbuilderupdate;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        alertbuilderupdate = new AlertDialog.Builder(MainActivity.this, android.R.style.Theme_Material_Light_Dialog_Alert);
                    } else {
                        alertbuilderupdate = new AlertDialog.Builder(MainActivity.this);
                    }
                    alertbuilderupdate.setCancelable(false);
                    // String message = "A new version of "+ getResources().getString(R.string.app_name)+" is available. Please update to version "+latestVersion +" "+ getResources().getString(R.string.youAreNotUpdatedMessage1);
                    String message = "A new version of this app is available. Please update to version " + latestVersion + " " + getResources().getString(R.string.youAreNotUpdatedMessage1);
                    alertbuilderupdate.setTitle(getResources().getString(R.string.youAreNotUpdatedTitle))
                            .setMessage(message)
                            .setPositiveButton(getResources().getString(R.string.update), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                    Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=com.linkmatm.service&hl=en_US");

                                    startActivity(new Intent(Intent.ACTION_VIEW, uri));
                                    dialog.dismiss();
                                }
                            })
                            .show();
                }
            });
        }

    }

}