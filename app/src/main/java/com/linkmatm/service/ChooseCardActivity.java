package com.linkmatm.service;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

public class ChooseCardActivity extends AppCompatActivity {

    public static ChooseCardActivity instance;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_card);
        instance=this;
    }
    void doFinish(){
        instance.finish();
    }

//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
//        Constants.isPos = "2";
//        Intent intent = new Intent();
//        intent.putExtra("errorResponse", "App closed by user during transaction");
//        Constants.intentData = intent;
//        doFinish();
//        Matm2Activity.instance.finish();
//        Matm2Activity.instance.shutDownTransaction();
//    }
}